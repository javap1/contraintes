package planning;

import java.util.*;
import representation.*;

public class BFSPlanner implements Planner{
	
	private Map<Variable, Object> InitialState;
	private Set<Action> Actions;
	private Goal goal;
	private int nd=0;

	public BFSPlanner(Map<Variable, Object> Init, Set<Action> Act,Goal g) {
		this.InitialState = Init;
		this.Actions = Act;
		this.goal = g;
	}


        public List<Action> plan()
        {  
             BFSPlanner problem = this;
             Map<Map<Variable, Object>,Action> plan = new  HashMap<Map<Variable, Object>,Action>();
             Map<Map<Variable, Object>,Map<Variable,Object>> father = new HashMap<Map<Variable, Object>,Map<Variable,Object>>();
             List<Map<Variable, Object>> closed = new ArrayList<Map<Variable, Object>>();
             closed.add(problem.InitialState);
             List<Map<Variable, Object>> open = new ArrayList<Map<Variable, Object>>();
             open.add(problem.InitialState);
             
             
             father.put(problem.InitialState,null);
             
             if(problem.goal.isSatisfiedBy(problem.InitialState))
               return new ArrayList<Action>();
             Map<Variable, Object> next = new HashMap<Variable, Object>();
             Map<Variable, Object> instantiation = new HashMap<Variable, Object>();  
             if(!open.isEmpty())
               this.nd=1;
               
             while(!open.isEmpty())
             {                    
                instantiation = open.remove(0);
                closed.add(instantiation);
                for(Action act : problem.Actions)
                 {
                      if( act.isApplicable(instantiation))
                       { nd++;
                         next = act.successor(instantiation);
                         if(!closed.contains(next) && !open.contains(next))
                             {
                              
                              father.put(next,instantiation);
                              plan.put(next,act);
                              if(problem.goal.isSatisfiedBy(next))
                                 return get_bfs_plan(father,plan,next); 
                              else
                                open.add(next);
                             }
                       }
                 } 

              }
               return null;
         
        
        
        }
        
        
        
        
        
        
        public List<Action> get_bfs_plan(Map<Map<Variable, Object>,Map<Variable,Object>> father , Map<Map<Variable, Object>,Action> plan , Map<Variable, Object> goal ){
             Stack<Action> bfs_plan = new Stack<Action>(); 
             while(goal != null)
              {bfs_plan.push(plan.get(goal));
               goal=father.get(goal);
              }
               
            
             Collections.reverse(bfs_plan);
              bfs_plan.remove(0);   
             return bfs_plan;
    
        
        
        
        }
        
        
    public int getnbd()
       { return this.nd;
       }
        
	@Override
	public Map<Variable, Object> getInitialState() {
		
		return this.InitialState;
	}

	@Override
	public Set<Action> getActions() {
		
		return this.Actions;
	}

	@Override
	public Goal getGoal() {
		
		return this.goal;
	}
	

	

}
