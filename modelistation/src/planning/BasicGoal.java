package planning;

import java.util.Map;
import representation.*;

public class BasicGoal implements Goal{

	  private Map<Variable, Object> but;
     
      
	public BasicGoal (Map<Variable, Object> b) {
		this.but=b;
	}
	
	public Map<Variable, Object> getGoal()
	{  return this.but;    }
	
	public boolean isSatisfiedBy(Map<Variable, Object> state) {
		
		for(Variable var : but.keySet()) {
		        if(!state.containsKey(var))
		              return false;
			if(!this.but.get(var).equals(state.get(var)))
				return false;
			
		}
		return true;
	}

	}
