package planning;


import java.util.*;
import representation.*;

/**
 * Cette classe decrit un planificateur qui utilise l'algorithme DFS (Deep First
 * Search), permettant de faire de la recherche de chemin en profondeur dans un
 * graphe.
 */

public class DFSPlanner implements Planner{
	
	 /**
     * etat initial du planificateur.
     */
	private Map<Variable, Object> InitialState;

    /**
     * Ensemble d'actions pouvant etre effectuees.
     */
	private Set<Action> Actions;
	
	/**
     * But a atteindre.
     */
	private Goal goal;
	/**
     * Nombre de noeuds explores.
     */
    private int nd=0;
	
	public DFSPlanner(Map<Variable, Object> Init, Set<Action> Act,Goal g) {
		this.InitialState = Init;
		this.Actions = Act;
		this.goal = g;
	}


        public List<Action> dfs(DFSPlanner problem,Map<Variable, Object> instantiation,Stack<Action> plan,List<Map<Variable, Object>> closed )
        {

             closed.add(problem.InitialState);
             
             Map<Variable, Object> next = new HashMap<Variable, Object>();
             List<Action> subplan = new ArrayList<Action>();
             if(problem.goal.isSatisfiedBy(instantiation))
               return plan;
             else
              { for(Action act : problem.Actions)
                 {
                      if( act.isApplicable(instantiation))
                       { next = act.successor(instantiation);

                         if(!closed.contains(next))
                             { nd++;
                              plan.push(act);
                              closed.add(next);
                              subplan = dfs(problem,next,plan,closed);
                              if(subplan!=null)
                                return subplan;
                              else
                                plan.pop();
                             }
                       }
                 }

              return null;
              }




        }

        public List<Action> plan(){
             Stack<Action> plan = new Stack<Action>();
             List<Map<Variable, Object>> closed = new ArrayList<Map<Variable, Object>>();

             return dfs(this,this.InitialState,plan,closed);



        }


       public int getnbd()
       { return this.nd;
       }

	@Override
	public Map<Variable, Object> getInitialState() {
		
		return this.InitialState;
	}

	@Override
	public Set<Action> getActions() {
		return this.Actions;
	}

	@Override
	public Goal getGoal() {
		return this.goal;
	}




}
