package planning ;


import java.util.*;
import representation.*;
public class Demo{
 
     public static void main(String[] args)     
       {
       
 Set<Object> sob = new HashSet<Object>();
       sob.add("1");sob.add("2");sob.add("3");
       Map<Variable, Object> inst =Map.of(
 new Variable("11", sob), "1",
 new Variable("22", sob), "2",
 new Variable("33", sob), "3");
 
 BasicAction A1 = new BasicAction(
 Map.of(
 new Variable("11", sob), "1",
 new Variable("22", sob), "2"
 ),
 Map.of(
 new Variable("11", sob), "1",
 new Variable("22", sob), "2"
 ),
 (int) (10)
 );
 
 BasicAction A2 = new BasicAction(
 Map.of(
 new Variable("11", sob), "2",
 new Variable("22", sob), "3"
 ),
 Map.of(
 new Variable("11", sob), "3",
 new Variable("22", sob), "1"
 ),
 (int) (11)
 );
 
 BasicAction A3 = new BasicAction(
 Map.of(
 new Variable("11", sob), "3",
 new Variable("22", sob), "1"
 ),
 Map.of(
 new Variable("11", sob), "1",
 new Variable("22", sob), "2"
 ),
 (int) (12)
 );
 
 BasicAction A4 = new BasicAction(
 Map.of(
 new Variable("11",sob), "1",
 new Variable("22", sob), "3"
 ),
 Map.of(
 new Variable("11", sob), "2",
 new Variable("22", sob), "1"
 ),
 (int) (13)
 );
 
 BasicAction A5 = new BasicAction(
 Map.of(
 new Variable("11", sob), "2",
 new Variable("22", sob), "1"
 ),
 Map.of(
 new Variable("11", sob), "3",
 new Variable("22", sob), "2"
 ),
 (int) (14)
 );
 
 BasicAction A6 = new BasicAction(
 Map.of(
 new Variable("11", sob), "3",
 new Variable("22", sob), "2"
 ),
 Map.of(
 new Variable("11", sob), "1",
 new Variable("22", sob), "3"
 ),
 (int) (15)
 );
 

 
 BasicAction A7 = new BasicAction(
 Map.of(
 new Variable("11", sob), "1",
 new Variable("22", sob), "2"
 ),
 Map.of(
 new Variable("11", sob), "1",
 new Variable("22", sob), "3"
 ),
 (int) (17)
 );
 
 Set<Action> actions=Set.of(A1,A2,A3,A4,A5,A6,A7);
 
 
 

 Goal goal=new BasicGoal(
 Map.of(
 new Variable("11", sob), "2",
 new Variable("22", sob), "1",
 new Variable("33", sob), "3"));


 BFSPlanner prblm = new BFSPlanner(inst,actions,goal);
 prblm.plan();
 System.out.println("nb noeud BFS :"+prblm.getnbd());
 
 DFSPlanner prOblm = new DFSPlanner(inst,actions,goal);
 prOblm.plan();
 System.out.println("nb noeud dFS :"+prOblm.getnbd());

 DijkstraPlanner prbElm = new DijkstraPlanner(inst,actions,goal);
 prbElm.plan();
 System.out.println("nb noeud dij :"+prbElm.getnbd());
 
 
       
       
       
       
       
       
       
       
       }
       
 }
