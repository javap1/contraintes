package planning;

import java.util.*;
import representation.Variable;

public class DijkstraPlanner implements Planner{

	private Map<Variable, Object> InitialState;
	private Set<Action> Actions;
	private Goal goal;
        private int nd=0;

	public DijkstraPlanner(Map<Variable, Object> Init, Set<Action> Act,Goal g) {
			this.InitialState = Init;
			this.Actions = Act;
			this.goal = g;
		}



      public Map<Variable, Object> argmin(List<Map<Variable, Object>> open ,Map<Map<Variable, Object>,Float> distance )
         {   Map<Variable, Object> instantiation = new HashMap<Variable, Object>();
            if(open.isEmpty())
              return null;
            instantiation = open.get(0);
            for (Map<Variable, Object> State : open)
              if(distance.containsKey(State) && distance.containsKey(instantiation) )
		if(distance.get(State) < distance.get(instantiation))
		    instantiation = State;

            return instantiation;
         }

      public List<Action> plan()
         {

	   DijkstraPlanner problem = this;
	   Map<Map<Variable, Object>,Action> plan = new  HashMap<Map<Variable, Object>,Action>();
	   Map<Map<Variable, Object>,Float> distance = new HashMap<Map<Variable, Object>,Float>();
	   Map<Map<Variable, Object>,Map<Variable, Object>> father = new HashMap<Map<Variable, Object>,Map<Variable, Object>>();
	   List<Map<Variable, Object>> goals = new ArrayList<Map<Variable, Object>>();
	   List<Map<Variable, Object>> open = new ArrayList<Map<Variable, Object>>();
	   father.put(problem.InitialState,null);
	   distance.put(problem.InitialState,(float) 0);
	   open.add(problem.InitialState);

	   Map<Variable, Object> instantiation = new HashMap<Variable, Object>();
	   Map<Variable, Object> next = new HashMap<Variable, Object>();
           if(!open.isEmpty())
           
	             while(!open.isEmpty())
	             {

	            	instantiation=argmin(open,distance);
	            	open.remove(instantiation);


	            	if(problem.goal.isSatisfiedBy(instantiation))
	            		goals.add(instantiation);


	            	 for(Action act : problem.Actions)
	            	  {
	            		 if( act.isApplicable(instantiation))
	            		 {    nd++;
	            			 next = act.successor(instantiation);
	            			 if(!distance.containsKey(next))
                                {distance.put(next,Float.MAX_VALUE);}

	            			 if(distance.get(next) > (distance.get(instantiation) + act.getCost()) )
	            			 {
	            				 distance.put(next, distance.get(instantiation)+act.getCost());
	            				 father.put(next,instantiation);
	            				 plan.put(next, act);
	            				 open.add(next);
	            			 }
	            		 }
	            	 }

	             }

	          if(goals.isEmpty())
	        	  return null;
	          else
	        	  return get_dijkstra_plan(father,plan,goals,distance);


	    }

      public List<Action> get_dijkstra_plan(Map<Map<Variable, Object>,Map<Variable,Object>> father , Map<Map<Variable, Object>,Action> plan , List<Map<Variable, Object>> goals , Map<Map<Variable, Object>, Float> distance ){

	              Stack<Action> DIJ_plan = new Stack<Action>();
	               Map<Variable, Object> goal = argmin(goals,distance);

	              while(goal != null)
	               {
	                 DIJ_plan.push(plan.get(goal));
	                 goal=father.get(goal);
	               }
	              Collections.reverse(DIJ_plan);
	              DIJ_plan.remove(0);
	              return DIJ_plan;

	        }

       public int getnbd()
       { return this.nd;
       }
       
       @Override
	public Map<Variable, Object> getInitialState() {
		return this.InitialState;
	}

	@Override
	public Set<Action> getActions() {
		return this.Actions;
	}

	@Override
	public Goal getGoal() {
		return this.goal;
	}







}
