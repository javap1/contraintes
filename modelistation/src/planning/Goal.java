package planning;

import java.util.Map;
import representation.*;

public interface Goal {

	
	public boolean isSatisfiedBy(Map<Variable,Object> state);
	
}
