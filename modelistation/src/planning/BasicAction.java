package planning;

import java.util.*;

import representation.*;

public class BasicAction implements Action {
       
       private Map<Variable, Object> pre;
       private Map<Variable, Object> effect;
       private int cost;
       
       
	public BasicAction (Map<Variable, Object> p, Map<Variable, Object> e, int c) {
		this.pre=p;
		this.effect=e;
		this.cost=c;
	}
	
	
	
	@Override
	public boolean isApplicable(Map<Variable, Object> etat) {
		
		
		
		for(Variable var : pre.keySet()) 
		      { if(!etat.containsKey(var))
		               return false;
			if( this.pre.get(var) != etat.get(var)) 
				return false;
			}
			
		return true;
	}
	
	
	
	@Override
	public Map<Variable, Object> successor(Map<Variable, Object> ss) {
		
		Map<Variable, Object> s = new HashMap<Variable, Object>(ss);
		
		  if(this.isApplicable(s))
		    {
		        for(Variable var : this.effect.keySet()) 
			       s.put(var,this.effect.get(var)); 
		    }
		return s;
	}
	
	 public String toString()
	    {
	        return "p:"+this.pre+"\n e:"+this.effect+"";
	    }
	
	
	@Override
	public int getCost() {
		
		return this.cost;
	}

   
	
}
