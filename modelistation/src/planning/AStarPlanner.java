package planning;

import java.util.*;
import representation.Variable;

/**
 * Cette classe decrit un planificateur qui utilise l'algorithme A*, permettant
 * de faire de la recherche de chemin.
 */

public class AStarPlanner implements Planner{

   /**
     * etat initial du planificateur.
     */
    private final Map<Variable, Object> InitialState;

    /**
     * But a atteindre.
     */
    private final Goal goal;
   /**
     * Ensemble d'actions pouvant etre effectuees.
     */
	private Set<Action> Actions;
   /**
     * Heuristique utilise pour optimiser l'algorithme de recherche de plan.
     */
	private Heuristic h;
   /**
     * Nombre de noeuds explores.
     */
	private int nd =0;
   /**
     * Constructeur par defaut.
     * 
     * @param initialState etat initial
     * @param actions      ensemble d'actions pouvant etre effectuees
     * @param goal         but a atteindre
     * @param heuristic    heuristique
     */
	public AStarPlanner(Map<Variable, Object> Init, Set<Action> Act,Goal g,Heuristic hh) {
			this.InitialState = Init;
			this.Actions = Act;
			this.goal = g;
			this.h=hh;
		}

	  @Override
	    public Map<Variable, Object> getInitialState() {
	        return this.InitialState;
	    }

	    @Override
	    public Set<Action> getActions() {
	        return this.Actions;
	    }

	    @Override
	    public Goal getGoal() {
	        return this.goal;
	    }
	    
	    public int getnbd(){ 
	    	return this.nd;
	       }
	    
	    
	    
	    
      public Map<Variable, Object> argmin(List<Map<Variable, Object>> open ,Map<Map<Variable, Object>,Float> distance )
         {   Map<Variable, Object> instantiation = new HashMap<Variable, Object>();
            if(open.isEmpty())
              return null;
            instantiation = open.get(0);
            for (Map<Variable, Object> State : open)
              if(distance.containsKey(State) && distance.containsKey(instantiation) )
		if(distance.get(State) < distance.get(instantiation))
		    instantiation = State;

            return instantiation;
         }


      public List<Action> plan(){
	
		   AStarPlanner problem = this;
		   Map<Map<Variable, Object>,Action> plan = new  HashMap<Map<Variable, Object>,Action>();
		   Map<Map<Variable, Object>,Float> distance = new HashMap<Map<Variable, Object>,Float>();
		   Map<Map<Variable, Object>,Map<Variable, Object>> father = new HashMap<Map<Variable, Object>,Map<Variable, Object>>();
		   Map<Map<Variable, Object>,Float> value = new HashMap<Map<Variable, Object>,Float>();
		   List<Map<Variable, Object>> open = new ArrayList<Map<Variable, Object>>();
		   father.put(problem.InitialState,null);
		   distance.put(problem.InitialState,(float) 0);
		   open.add(problem.InitialState);
		   value.put(problem.InitialState,this.h.estimate(problem.InitialState));
	
		   Map<Variable, Object> instantiation = new HashMap<Variable, Object>();
		   Map<Variable, Object> next = new HashMap<Variable, Object>();
		   
		   if(!open.isEmpty())
	            
		             while(!open.isEmpty())
		             {
	
		            	instantiation=argmin(open,value);
		            	
	
	
		            	if(problem.goal.isSatisfiedBy(instantiation))
		            		return get_bfs_plan(father,plan,instantiation);
	                  else
	                  	open.remove(instantiation);
	
		            	 for(Action act : problem.Actions)
		            	  {
		            		 if( act.isApplicable(instantiation))
		            		 {    nd++;
		            			 next = act.successor(instantiation);
	
		            			 if(!distance.containsKey(next)){
		            				
		            				 distance.put(next,Float.MAX_VALUE);
		            				 }
	
		            			 if(distance.get(next) > (distance.get(instantiation) + act.getCost()) )
		            			 {
		            				 distance.put(next, distance.get(instantiation)+act.getCost());
		            				 value.put(next, distance.get(next) + h.estimate(next));
		            				 father.put(next,instantiation);
		            				 plan.put(next, act);
		            				 open.add(next);
		            			 }
		            		 }
		            	 }
	
		             }
	
		         
		        	  return null;
	
	
		    }

      public List<Action> get_bfs_plan(Map<Map<Variable, Object>,Map<Variable,Object>> father , Map<Map<Variable, Object>,Action> plan , Map<Variable, Object> goal ){
             Stack<Action> bfs_plan = new Stack<Action>(); 
             while(goal != null)
              {bfs_plan.push(plan.get(goal));
               goal=father.get(goal);
              }
               
            
             Collections.reverse(bfs_plan);
              bfs_plan.remove(0);   
             return bfs_plan;
    
        
        
        
        }





}
