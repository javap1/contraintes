package representation ;
import java.util.*;





public class Implication implements Constraint {
   
   public Variable v1,v2;
   public Set<Object> s1,s2;
  

   public Implication(Variable vv,Set<Object> ss1,Variable v,Set<Object> ss2)
     {   this.v1=vv;
         this.v2=v;
         this.s1=ss1;
         this.s2=ss2;
     
     }

   public Variable getV1() {
       return v1;
   }

   public Variable getV2() {
       return v2;
   }

   public Set<Object> getS1() {
       return s1;
   }

   public Set<Object> getS2() {
       return s2;
   }
   
 @Override 
 public Set<Variable> getScope(){ 
 	  HashSet<Variable> r = new HashSet<Variable>();
 	  r.add(v1);
 	  r.add(v2);
         return r;
      }

 @Override  
  public boolean isSatisfiedBy(Map<Variable,Object> map)
     {  
        for (Variable var : map.keySet())
          { if(map.get(var) == null) 
              throw new IllegalArgumentException("Exist(valeur,null)") ;
           }
           
           
        if(!(map.containsKey(v1) && map.containsKey(v2)))
            throw new IllegalArgumentException("v1 ou v2 doesn't exist in Map") ;
           
            
        Object o= map.get(v1);
        if(s1.contains(o))
         return s2.contains(map.get(v2)) ;
        return true;
     }

@Override
public String toString() {
	return "Implication [v1=" + v1.toString() + ", v2=" + v2.toString() + ", s1=" + s1 + ", s2=" + s2 + "]";
}









}
