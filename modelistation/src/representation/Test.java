package representation ;

import representationtests.VariableTests ;
import representationtests.BooleanVariableTests ;
import representationtests.ImplicationTests;
import representationtests.DifferenceConstraintTests;
import representationtests.UnaryConstraintTests;
public class Test{
 
     public static void main(String[] args)     
       {
		boolean ok = true ;
		ok = ok && VariableTests.testGetName();
		ok = ok && VariableTests.testGetDomain();
		ok = ok && VariableTests.testEquals();
		ok = ok && VariableTests.testHashCode();
		ok = ok && BooleanVariableTests.testConstructor();
		ok = ok && BooleanVariableTests.testEquals();
		ok = ok && BooleanVariableTests.testHashCode();
		
		 System.out.println( ok ? " All tests OK " : " At least one test KO ");
	       
		 ok = ok && DifferenceConstraintTests.testGetScope();
         ok = ok && DifferenceConstraintTests.testIsSatisfiedBy();
         ok = ok && UnaryConstraintTests.testGetScope();
         ok = ok && UnaryConstraintTests.testIsSatisfiedBy();
         ok = ok && ImplicationTests.testIsSatisfiedBy();
         ok = ok && ImplicationTests.testGetScope(); 
               
    
               System.out.println( ok ? " All tests OK " : " At least one test KO ");
       }
}
