package representation ;
import java.util.*;



/**
 * Cette classe decrit une variable utilisable.
 */

public class Variable {

	 /**
     * Nom de la variable.
     */
    public String nom;
    /**
     * Ensemble de valeurs possible pour instancier cette variable.
     */
    public Set<Object> domaine;
    
    /**
     * Conctructeur par defaut.
     * 
     * @param name   nom de la variable
     * @param domain ensemble de valeurs possibles pour instancier la variable
     */
    
    public Variable(String n, Set<Object> d)
    {
            this.nom = n;
            this.domaine=d;
    }

    
    public String getName(){
      return this.nom;
    }
    
    
    public Set<Object> getDomain(){
      return this.domaine;
    }
    
    
    
    public boolean equals(Object o)
      {   
          if(o instanceof Variable && o!=null)
          {
              Variable v = (Variable) o;
              return this.nom.equals(v.getName());
              
          }                      
          return false;
        }


    public int hashCode()
      {   
          return this.nom.hashCode();
        }

    
    
    
	@Override
	public String toString() {
		return "Variable [nom=" + this.getName() + ", domaine=" + this.toStringDomain() + "]";
	}
   
	public String toStringDomain(){
	       String s = new String();
	       for (Object o : this.getDomain() )
	    	  s+= o + "  ";
	       return s;
	      }

   
} 
