package representation ;
import java.util.*;



/**
 * Cette classe decrit une variable de type booleenne.
 */

public class BooleanVariable extends Variable {

    public String nom;
   
    
    /**
     * Constructeur par defaut.
     * 
     * @param name nom de la variable
     */
    public BooleanVariable(String name){
        super(name, new HashSet<>(Arrays.asList(true, false)));
    }


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	@Override
	public String toString() {
		return "BooleanVariable [nom=" + nom + "]";
	}
       
       

 





























}

