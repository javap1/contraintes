package representation ;
import java.util.*;



/**
 * Cette classe decrit une contrainte de type "differente".
 */

public class DifferenceConstraint implements Constraint {

	/**
     * Variables utilisees par la contrainte.
     */
    public Variable v1;
    public Variable v2;
    
    /**
     * Constructeur par defaut.
     * 
     * @param v1 premiere variable
     * @param v2 deuxieme variable
     */
    public DifferenceConstraint(Variable vv,Variable v){
    
       this.v1=vv;
       this.v2=v;
    
    }
    
   
     public Set<Variable> getScope(){ 
         return new HashSet<Variable>(Arrays.asList(this.v1, this.v2));
      }
   
   
     public boolean isSatisfiedBy(Map<Variable,Object> map)
     { 
    // verifie si l'affectation contient les deux variables de la contrainte
        for (Variable var : map.keySet())
          { if(map.get(var) == null) 
              throw new IllegalArgumentException("E(valeur,null)") ;
           }
           
           
        if(!(map.containsKey(v1) && map.containsKey(v2)))
            throw new IllegalArgumentException("v1 ou v2 doesn't exist in Map") ;
            
            
        Object o= map.get(v1);
        return !o.equals(map.get(v2)) ;
     
     }
         


     public Variable getV1() {
         return v1;
     }

     public Variable getV2() {
         return v2;
     }

     public String toString() {
         return "DifferenceConstraint[" + this.getV1() + " != " + this.getV2() + "]";
     }



}
