package representation ;
import java.util.*;





public class UnaryConstraint implements Constraint {

   public Variable v;
   public Set<Object> s;


   public UnaryConstraint(Variable vv,Set<Object> ss)
     {
         this.v=vv;
         this.s=ss;

     }

   public Variable getV1() {
       return v;
   }
   public Set<Object> getS1() {
       return s;
   }
    public Set<Variable> getScope(){
    	Set<Variable> h = new HashSet<Variable>();
    	h.add(v);
         return h;
      }
    
    

     public boolean isSatisfiedBy(Map<Variable,Object> map)
     {
        for (Variable var : map.keySet())
          { if(map.get(var) == null)
              throw new IllegalArgumentException("Exist(valeur,null)") ;
           }


        if(!map.containsKey(v) )
            throw new IllegalArgumentException("v doesn't exist in Map") ;


         Object o= map.get(v);
         return s.contains(o) ;

     }

	public Variable getV() {
		return v;
	}

	public void setV(Variable v) {
		this.v = v;
	}

	public Set<Object> getS() {
		return s;
	}

	public void setS(Set<Object> s) {
		this.s = s;
	}

	@Override
	public String toString() {
		return "UnaryConstraint [v=" + v + ", s=" + s + "]";
	}













   }
