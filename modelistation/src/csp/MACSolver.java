package csp ;


import java.util.*;
import representation.*;

/**
 * Cette classe est un solveur de contraintes qui verifie l'arc-coherence des
 * domaines de variables pour optimiser la recherche de solution.
 */

public class MACSolver extends AbstractSolver {

	/**
     * Ensemble de variables du probleme.
     */
  private Set<Variable> variables;

	/**
	 * Ensemble de contraintes a satisfaire.
	 */
  private Set<Constraint> contraintes;

	/**
	 * Constructeur par defaut.
	 * 
	 * @param vr   ensemble de variables
	 * @param cn ensemble de contraintes
	 */
	public MACSolver(Set<Variable> vr,Set<Constraint> cn)
	 { super(vr,cn);
	   this.variables=vr;
	   this.contraintes= cn;
	 }

 
	 public Map<Variable, Object> mac(Map<Variable, Object> I,LinkedList<Variable> V,Map<Variable, Set<Object>> map)
	 {
	     if(V.isEmpty())
	      return I;
	     ArcConsistency arc = new ArcConsistency(this.contraintes);
	     if(!arc.ac1(map))
	          return null;
	     Variable xi = V.remove();
	     for(Object vi : map.get(xi))
	        { Map<Variable, Object> N = new HashMap<Variable, Object>(I);
	          N.put(xi,vi);
	          if(isConsistent(N))
	           {
	              Map<Variable, Object> R = mac(N,V,map);
	              if(R!=null)
	               return R;
	           }
	
	        }
	      V.add(xi);
	      return null;
	 }
	
	public Map<Variable, Object> solve(){
	   Map<Variable, Set<Object>> ED =new HashMap<Variable, Set<Object>>();
	   for(Variable v :this.variables)
	    ED.put(v,v.getDomain());
	  return mac(new  HashMap<Variable,Object>(),new LinkedList<Variable>(this.variables),ED);
	
	
	}
	
	
	public Set<Variable> getVariables() {
		return variables;
	}


	public void setVariables(Set<Variable> variables) {
		this.variables = variables;
	}


	public Set<Constraint> getContraintes() {
		return contraintes;
	}


	public void setContraintes(Set<Constraint> contraintes) {
		this.contraintes = contraintes;
	}


	public String VartoString() {
		String s = new String();
		for (Variable v : variables)
			s+=v.toString() + " ";
		return s;	
	}
	
	public String ConstoString() {
		String s = new String();
		for (Constraint c : contraintes)
			s+=c.toString() + " ";
		return s;	
	}

	@Override
	public String toString() {
		return "MACSolver [variables=" + VartoString() + ", contraintes=" + ConstoString() + "]";
	}
	

	
	}
