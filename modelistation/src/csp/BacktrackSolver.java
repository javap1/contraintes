package csp ;
import java.util.*;
import representation.*;





public class BacktrackSolver extends AbstractSolver {
	
	/**
     * Ensemble de variables du probleme.
     */
	
    private Set<Variable> variables;
    
    /**
     * Constructeur par defaut.
     * 
     * @param vr   ensemble de variables
     * @param cn ensemble de contraintes
     */
   public BacktrackSolver(Set<Variable> vr,Set<Constraint> cn)
     { super(vr,cn);
       variables=vr;
       
     }

     public Map<Variable, Object> solve(){

      return BT(new HashMap<Variable, Object>(),new LinkedList<Variable>(this.variables));


     }
     /**
      * 
      * 
      * @param I            instanciation vide ou partielle
      * @param V 			liste de variables non instanciees
      * @return une instanciation partielle, totale ou vide (dependant de l'execution
      *         de l'agorithme sur les contraintes du probleme)
      * @see AbstractSolver#variables
      * @see AbstractSolver#constraints
      */

  public Map<Variable, Object> BT(Map<Variable, Object> I,LinkedList<Variable> V){

       if(V.isEmpty())
         return I;
       Map<Variable, Object> R;
       Variable x = V.remove();
       for(Object obj : x.getDomain())
        {  Map<Variable, Object> N = new HashMap<Variable, Object>(I);
           N.put(x,obj);
           if(this.isConsistent(N))
            {  R= BT(N,V);
               if(R!=null)
                return R;
            }

        }
        V.add(x);
        return null;
    }

	public Set<Variable> getVariables() {
		return variables;
	}

	public void setVariables(Set<Variable> variables) {
		this.variables = variables;
	}
	public String VartoString() {
		String s = new String();
		for (Variable v : variables)
			s+=v.toString() + " ";
		return s;	
	}
	
	

	@Override
	public String toString() {
		return "BacktrackSolver [variables=" + VartoString() + "]";
	}



   







}
