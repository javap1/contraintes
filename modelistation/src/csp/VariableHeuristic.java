package csp;

import java.util.*;

import representation.*;

/**
 * Interface decrivant une heuristic capable d'optimiser les decouvertes sur les
 * variables lors de la resolution d'un probleme.
 */
public interface VariableHeuristic {
	
	/**
     * Retourne la meilleure variable au sens de l'heuristique.
     * 
     * @param variables ensemble de variables
     * @param domaines   ensemble de domaines
     * @return meilleure variable au sens de l'heuristique
     */
	public Variable best(Set<Variable> variables,Map<Variable, Set<Object>> domaines);

}
