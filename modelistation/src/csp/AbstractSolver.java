package csp ;
import java.util.*;
import representation.*;



	/**
	 * Cette classe abstraite decrit la base d'implementation d'un solveur de
	 * contraintes.
	 */
	public abstract class AbstractSolver implements Solver{

		/**
	     * Ensemble de variables du probleme.
	     */
		protected Set<Variable> variables;
		
	   /**
	     * Ensemble de contraintes a satisfaire par le solveur.
	     */
		protected Set<Constraint> contraintes;
		/**
	     * Constructeur par defaut.
	     * 
	     * @param vr  ensemble de variables du probleme
	     * @param cn ensemble de contraintes a satisfaire
	     */
	  public AbstractSolver(Set<Variable> vr,Set<Constraint> cn){
	    this.variables = vr ;
			this.contraintes = cn;

	  }

	  public abstract Map<Variable, Object> solve();
	  
	  /**
	   * Retourne si l'affectation passee en argument verifie toutes les contraintes
	   * qui portent sur les variables.
	   * 
	   * @param affectation affectation partielle
	   * @return si l'affectation est coherente avec les contraintes et les variables
	   */
		public boolean isConsistent(Map<Variable,Object> map ){

			 for(Constraint cn : this.contraintes)
			   { if(map.keySet().containsAll(cn.getScope()) )
					   if(!cn.isSatisfiedBy(map))
				       return false;}
			 return true ;


		}

	public Set<Variable> getVariables() {
		return variables;
	}

	public void setVariables(Set<Variable> variables) {
		this.variables = variables;
	}

	public Set<Constraint> getContraintes() {
		return contraintes;
	}

	public void setContraintes(Set<Constraint> contraintes) {
		this.contraintes = contraintes;
	}
	
	public String VartoString() {
		String s = new String();
		for (Variable v : variables)
			s+=v.toString() + " ";
		return s;	
	}
	
	public String ConstoString() {
		String s = new String();
		for (Constraint c : contraintes)
			s+=c.toString() + " ";
		return s;	
	}
	@Override
	public String toString() {
		return "AbstractSolver [variables=" + VartoString() + ", contraintes=" + ConstoString() + "]";
	}








}
