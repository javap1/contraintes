package csp ;
import java.util.*;
import representation.*;



/**
 * Interface permettant d'implementer des solveurs de contraintes.
 */
public interface Solver {

	 /**
     * Resout un probleme a partir de contraintes et renvoie une solution.
     * 
     * @return solution au probleme (instanciation complete) ou {@code null} si
     *         aucune solution n'a ete trouvee
     */
	
   public Map<Variable, Object> solve();


}
