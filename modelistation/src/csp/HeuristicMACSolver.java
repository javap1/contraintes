package csp;

import java.util.*;
import representation.*;

/**
 * Cette classe est un solveur de contraintes permettant de resoudre un probleme
 * au sens de l'heuristique.
 */
public class HeuristicMACSolver extends AbstractSolver 
{
	/**
     * Heuristique sur les variables.
     */
    protected VariableHeuristic varHeuristic;
    
    /**
     * Heuristique sur les valeurs.
     */
    protected ValueHeuristic valueHeuristic;

    
    /**
     * Constructeur par defaut.
     * 
     * @param variables         ensemble de variables
     * @param constraints       ensemble de contraintes
     * @param variableHeuristic heuristique sur les variables
     * @param valueHeuristic    heuristique sur les valeurs
     */
    
    public HeuristicMACSolver(Set<Variable> variables, Set<Constraint> contraintes ,VariableHeuristic varHeuristic, ValueHeuristic valueHeuristic) {
        super(variables,contraintes);
        this.varHeuristic=varHeuristic;
        this.valueHeuristic=valueHeuristic;

    }
    
    
    public  Map<Variable, Object> solve()
    {
        Map<Variable,Set<Object>> domaine=new HashMap<Variable,Set<Object>>();
        for(Variable var:this.variables)
        {
            domaine.put(var,new HashSet<>(var.getDomain()));
        }
        return this.MacHeuristic(new HashMap<>(),new LinkedList<>(this.variables),domaine);
    }
    
    
    
    public  Map<Variable, Object> MacHeuristic(Map<Variable, Object> solution_partielle,LinkedList<Variable> variables,Map<Variable,Set<Object>> domaines)
    {
        if(variables.isEmpty())
        {
            return solution_partielle;
        }
        else
        {
            ArcConsistency arc=new ArcConsistency(this.contraintes);
            if(!arc.ac1(domaines))
            {
                return null;
            }
            Variable x=this.varHeuristic.best(new HashSet<>(variables),domaines);
            variables.remove(x);
            for(Object obj :this.valueHeuristic.ordering(x,domaines.get(x)))
            {
                Map<Variable, Object> n = new HashMap<>(solution_partielle);
                n.put(x,obj);
                if(this.isConsistent(n))
                {
                    Map<Variable, Object> r=new HashMap<>();
                    r.put(x,obj);
                    r=this.MacHeuristic(n,variables,domaines);
                    if(r!=null)
                    {
                        return r;
                    }
                }
            }
            variables.add(x);;
            return null;
        }

    }


	public VariableHeuristic getVarHeuristic() {
		return varHeuristic;
	}


	public void setVarHeuristic(VariableHeuristic varHeuristic) {
		this.varHeuristic = varHeuristic;
	}


	public ValueHeuristic getValueHeuristic() {
		return valueHeuristic;
	}


	public void setValueHeuristic(ValueHeuristic valueHeuristic) {
		this.valueHeuristic = valueHeuristic;
	}
    
    

}
