package csp ;


import java.util.*;
import representation.*;



	/**
	 * Cette classe decrit une arc-consistence locale sur des contraintes.
	 */
public class ArcConsistency{

   /**
     * Ensemble de contraintes a satisfaire.
     */
  private Set<Constraint> contraintes;
	 
  /**
   * Constructeur par defaut.
   * 
   * @param cn ensemble de contraintes
   */
  public ArcConsistency(Set<Constraint> cn){
    for(Constraint c : cn)
    {
     Set<Variable> scope = c.getScope();
     if ( !(scope.size()==1 || scope . size ()==2)) {
      throw new IllegalArgumentException( " Ni â�£ unaire â�£ ni â�£ binaire " );
    }
    this.contraintes=cn;
  }
}


  public boolean enforceNodeConsistency( Map<Variable,Set<Object>> map)
  {
     Set<Object> remve = new HashSet<Object>();
     Map<Variable,Object> nap = new HashMap<Variable,Object>();

     for(Variable x : map.keySet())
     {

      for(Object v : map.get(x) )
      { if(contraintes == null)
         break;
        for (Constraint c : contraintes)
        {
          if (c.getScope().size() ==1 && c.getScope().contains(x))
           {
             nap.put(x,v);
             if(!c.isSatisfiedBy(nap))
                {remve.add(v);
                 nap.remove(x,v); }
            }

           }
       }

       map.get(x).removeAll(remve);
       remve.clear();
   }

     for(Variable x : map.keySet())
       if(map.get(x).isEmpty())
         return false;

     return true;
  }

/*public boolean revise(Variable v1 , Set<Object> d1,Variable v2 , Set<Object> d2 )
{   Map<Variable,Object> nap = new HashMap<Variable,Object>();
    int s ;
    Set<Object> remve = new HashSet<Object>();

     for ( Object o1 : d1)
      { s=0;
        for (Object o2 : d2)
        { for (Constraint c : contraintes)
           {
                if(c.getScope().size()==2 && c.getScope().contains(v1) && c.getScope().contains(v2))
                  { nap.put(v1,o1);
                    nap.put(v2,o2);
                    if(!c.isSatisfiedBy(nap))
                       s=1;
                nap.clear();}
           }
         }

         if(s==1)
          { remve.add(o1); }
       }

    d1.removeAll(remve);
    if(!remve.isEmpty())
       return false;
    return true;

}*/

public boolean revise(Variable v1 , Set<Object> d1,Variable v2 , Set<Object> d2 )
{
		
		    boolean del=false,viable,toutSatisfait;
		
		    Set<Object> remve = new HashSet<Object>();
		
		     for ( Object o1 : d1)
		      { viable=false;
		        for (Object o2 : d2)
		        { toutSatisfait =true;
		          for (Constraint c : contraintes)
		           {    if(c.getScope().size()==2 && c.getScope().contains(v1) && c.getScope().contains(v2))
		                {  Map<Variable,Object> nap = new HashMap<Variable,Object>();
		                  nap.put(v1,o1);
		                  nap.put(v2,o2);
		                  if(!c.isSatisfiedBy(nap))
		                  {toutSatisfait =false;
		                   break;}
		                }
		           }
		           if(toutSatisfait)
		            {viable=true;
		             break;}
		         }
		
		         if(!viable)
		          { remve.add(o1);
		             del =true; }
		       }
		
		    d1.removeAll(remve);
		    return del ;
		}


public boolean ac1(Map<Variable, Set<Object>> map ){

		   if(!enforceNodeConsistency(map))
		     return false;
		
		   boolean change ;
		   do{
		     change = false;
		     for(Variable x : map.keySet())
		       for(Variable y : map.keySet())
		         if(!x.equals(y))
		           if(revise(x,map.get(x),y,map.get(y)))
		              change=true;
		   }while(change);
		
		  for(Variable x : map.keySet())
		    if(map.get(x).isEmpty())
		       return false;
		
		  return true;
		}


public Set<Constraint> getContraintes() {
	return contraintes;
}


public void setContraintes(Set<Constraint> contraintes) {
	this.contraintes = contraintes;
}


public String ConstoString() {
	String s = new String();
	for (Constraint c : contraintes)
		s+=c.toString() + " ";
	return s;	
}

@Override
public String toString() {
	return "ArcConsistency [contraintes=" + ConstoString() + "]";
}






}
