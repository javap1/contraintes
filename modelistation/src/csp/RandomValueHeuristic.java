package csp;

import java.util.*;

import representation.*;


/**
 * Cette classe permet de faire une ordonnance aleatoire de valeurs.
 */
public class RandomValueHeuristic implements ValueHeuristic {
	
	 /**
     * Instance d'un objet permettant d'utiliser un generateur pseudo-aleatoire.
     */
    private Random random;

    /**
     * Constructeur par defaut.
     * 
     * @param rand instance d'un objet permettant d'utiliser un generateur
     *               pseudo-aleatoire
     */
    public RandomValueHeuristic(Random rand) {
        this.random = rand;
    }

    @Override
    public List<Object> ordering(Variable variable, Set<Object> domaine) {
        if (domaine == null) {
            return new ArrayList<>();
        }
        List<Object> list = new ArrayList<>(domaine);
        Collections.shuffle(list, this.random);
        return list;
    }

	public Random getRandom() {
		return random;
	}

	

	
    
    
    
}
