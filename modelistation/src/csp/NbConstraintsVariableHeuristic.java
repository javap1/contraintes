package csp;

import java.util.*;
import representation.*;


/**
 * Cette classe permet de recuperer la meilleure variable parmi un ensemble de
 * variables en fonction du nombre de contraintes qui portent sur elle.
 */

public class NbConstraintsVariableHeuristic implements VariableHeuristic {
	 
	/**
     * Booleen representant la preference au niveau des variables. S'il est a
     * {@code true}, c'est que l'on prefere que les variables apparaissent dans le
     * plus de contraintes possibles. S'il est a {@code false}, c'est que l'on
     * prefere que les variables apparaissent dans le moins de contraintes
     * possibles.
     */
    private boolean most;
    
    /**
     * Ensemble de contraintes.
     */
    private Set<Constraint> constraints;

    
    /**
     * Constructeur par defaut.
     * 
     * 
     * @param constraints ensemble de contraintes
     * @param most        booleen representant une preference au niveau du choix de
     *                    classement
     */
    public NbConstraintsVariableHeuristic(Set<Constraint> constraints,boolean most) {

        this.constraints = constraints;
        this.most = most;
    }

    @Override
    public Variable best(Set<Variable> variables, Map<Variable, Set<Object>> domains) {
        Variable best=null;
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
       
         for (Variable var : variables) {
            int actualValue = 0;
             for (Constraint c : this.constraints) {
               if(!c.getScope().isEmpty())
                if (c.getScope().contains(var)) {
                    actualValue+=1;
                    
                }
            }
            if (this.most) {
               
                if (actualValue > max) {
                    max = actualValue;
                    best = var;
                }
            } else {
           
                if (actualValue < min) {
                    min = actualValue;
                    best = var;
                }
            }
        }
       
        return best;
    }


}
