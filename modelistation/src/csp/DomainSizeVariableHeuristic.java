package csp;

import java.util.*;
import representation.*;
	
	/**
	 * Cette classe permet de recuperer la meilleure variable parmi un ensemble de
	 * variables en fonction de la taille de leur domaine.
	 */

public class DomainSizeVariableHeuristic implements VariableHeuristic {
 
   /**
     * Booleen representant la preference au niveau des variables. S'il est a
     * {@code true}, c'est que l'on prefere que les variables aient le plus grand
     * domaine possible. S'il est a {@code false}, c'est que l'on prefere que les
     * variables aient le plus petit domaine possible.
     */
    private boolean greatest;

    /**
     * Constructeur par defaut.
     * 
     * 
     * @param greatest    booleen representant une preference au niveau du choix de
     *                    classement
     */
    public DomainSizeVariableHeuristic(boolean greatest) {
        
        this.greatest = greatest;
    }

    @Override
    public Variable best(Set<Variable> variables, Map<Variable, Set<Object>> domains) {
        Variable best = null;
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (Variable var : variables) {
        	
           int actualValue = domains.get(var).size();
           
                if (this.greatest) { 
        
                      if (actualValue > max) {
                           max = actualValue;
                           best = var;
                        }
                 } else {
               
                      if (actualValue < min) {
                           min = actualValue;
                           best = var;
                         }
                 }
        }
        return best;
    }

    
    public boolean isGreatest() {
        return this.greatest;
    }

	public void setGreatest(boolean greatest) {
		this.greatest = greatest;
	} 
   
    
}
