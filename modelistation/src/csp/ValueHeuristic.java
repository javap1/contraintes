package csp;

import java.util.*;

import representation.*;
/**
 * Interface decrivant une heuristic capable d'optimiser les decouvertes sur les
 * valeurs lors de la resolution d'un probleme.
 */
public interface ValueHeuristic {
	
   /**
     * Retourne une liste de valeurs ordonnees selon l'heuristique.
     * 
     * @param variable variable
     * @param domain   domaine de la variable
     * @return liste ordonnee au sens de l'heuristique
     */
	public List<Object> ordering(Variable variable, Set<Object> domain);
}
