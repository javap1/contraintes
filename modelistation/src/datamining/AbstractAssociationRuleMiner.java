package datamining;

import java.util.*;

import representation.*;

/**
 * Classe abstraite representant la base des extracteurs de regles.
 */
public abstract class AbstractAssociationRuleMiner implements AssociationRuleMiner {
	/**
     * Base de donnees transactionnelles.
     */
	
    protected BooleanDatabase database;

    /**
     * Constructeur par defaut.
     * 
     * @param database base de donnees transactionnelles
     */
    public AbstractAssociationRuleMiner(final BooleanDatabase database) {
        this.database = database;
    }

    @Override
    public BooleanDatabase getDatabase() {
        return this.database;
    }

    /**
     * Retourne la frequence d'un motif parmi un ensemble de motifs.
     * 
     * @param itemset  motif que l'on souhaite calculer sa frequence
     * @param itemsets ensemble de motifs
     * @return frequence de {@code itemset} dans {@code itemsets}
     */
    public static float frequency(Set<BooleanVariable> itemset, Set<Itemset> itemsets) {
        for (Itemset transaction : itemsets) {
            if (itemset.equals(transaction.getItems())) { 
                return transaction.getFrequency();
            }
        }
        return 0;
    }

    /**
     * Retourne la frequence d'un motif parmi un ensemble de motifs.
     * 
     * @param itemset  motif que l'on souhaite calculer sa frequence
     * @param itemsets ensemble de motifs
     * @return frequence de {@code itemset} dans {@code itemsets}
     */
    public static float confidence(Set<BooleanVariable> premise, Set<BooleanVariable> conclusion,
            Set<Itemset> itemsets) {
        float X = 0, YX = 0;
        for (Itemset itemset : itemsets) {
            Set<BooleanVariable> set = new HashSet<>(premise);
            set.addAll(conclusion);
            if (itemset.getItems().equals(premise)) {
                X = itemset.getFrequency();
            }
            if (itemset.getItems().equals(set)) {
                YX = itemset.getFrequency();
            }
        }
        return YX / X;
    }
}
