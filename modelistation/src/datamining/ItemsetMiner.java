package datamining;

import java.util.*;
/**
 * Interface decrivant un mineur de donnees.
 */
public interface ItemsetMiner {
	/**
     * Recupere une base de donnees transactionnelle booleenne.
     * 
     * @return base de donnees
     */
	
	public BooleanDatabase getDatabase();
	
	/**
     * Extrait un ensemble de motifs a partir des donnees d'une base de donnees.
     * 
     * @param minimalFrequence frequence minimal pour le filtrage des frequences des motifs
     * 
     * @return ensemble de motifs
     */
	public Set<Itemset> extract(float minimalFrequence);
	

}
