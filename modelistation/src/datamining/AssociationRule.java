package datamining;

import java.util.*;

import representation.*;

/**
 * Classe representant une regle d'association.
 */

public class AssociationRule {
	
	 /**
     * Represente la premisse de cette regle d'association.
     */
    private Set<BooleanVariable> premise;

    /**
     * Represente la conclusion de cette regle d'association.
     */
	private Set<BooleanVariable> conclusion;
	
	/**
     * Represente la frequence d'apparition de cette regle dans les transactions.
     */
    private float frequence;
    
    /**
     * Represente la confiance dans cette regle.
     */
    private float confidence;


    /**
     * Constructeur par defaut.
     * 
     * @param premise    premisse
     * @param conclusion conclusion
     * @param frequency  frequence d'apparition dans la base de donnees
     * @param confidence confiance de la regle
     */
    public AssociationRule(Set<BooleanVariable> premise, Set<BooleanVariable> conclusion, float frequence,float confidence) {
        this.premise = premise;
        this.conclusion = conclusion;
        this.frequence = frequence;
        this.confidence = confidence;
    }

    /**
     * Recupere la premisse de la regle.
     * 
     * @return premisse
     * @see #premise
     */
    public Set<BooleanVariable> getPremise() {
        return this.premise;
    }

    /**
     * Recupere la conclusion de la regle.
     * 
     * @return conclusion
     * @see #conclusion
     */
    public Set<BooleanVariable> getConclusion() {
        return this.conclusion;
    }

    /**
     * Recupere la frequence d'apparition dans la base de donnees.
     * 
     * @return frequence
     * @see #frequency
     */
    public float getFrequency() {
        return this.frequence;
    }
    
    /**
     * Recupere la confiance dans cette regle.
     * 
     * @return confiance
     * @see #confidence
     */
    public float getConfidence() {
        return this.confidence;
    }
    
    public String ItemtoString(Set<BooleanVariable> set) {
		String s = new String();
		for (Variable v : set)
			s+=v.toString() + " ";
		return s;	
	
}
    
    public String toString() {
		return "AssociationRule [premise=" + this.ItemtoString(getPremise()) + ", conclusion=" + this.ItemtoString(getConclusion()) + ", frequence=" + frequence
				+ ", confidence=" + confidence + "]";
	}
}
