package datamining;

import java.util.*;
import representation.*;

/**
 * Classe representant un motif.
 */
public class Itemset {
	
	/**
     * Ensemble de variables.
     */
    private Set<BooleanVariable> items;
    
    /**
     * Frequence d'apparition de l'ensemble de variables dans la base de donnees
     * transactionnelle.
     */
    private float frequence;

    /**
     * Constructeur par defaut.
     * 
     * @param items     ensemble de variables
     * @param frequence frequence d'apparition de l'ensemble de variables dans la
     *                  base de donnees transactionnelle
     */
    public Itemset(Set<BooleanVariable> items,float frequence) {
        this.items = items;
        this.frequence = frequence;
    }

    /**
     * Recupere l'ensemble de variables.
     * 
     * @return ensemble de variables
     * @see #items
     */
    public Set<BooleanVariable> getItems() {
        return this.items;
    }

    /**
     * Recupere la frequence d'apparition de l'ensemble de variables dans la base de
     * donnees transactionnelle.
     * 
     * @return frequence d'apparition
     * @see #frequency
     */
    public float getFrequency() {
        return this.frequence;
    }
    
    public String ItemtoString() {
    		String s = new String();
    		for (Variable v : this.getItems())
    			s+=v.toString() + " ";
    		return s;	
    	
    }
    
	@Override
	public String toString() {
		return "Itemset [items="+ItemtoString()  + ", frequence=" + frequence + "]";
	}
}
