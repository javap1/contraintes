package datamining;

import java.util.*;

import representation.BooleanVariable;

/**
 * Classe representant un extracteur de regle utilisant un algorithme
 * <em>brute-force</em> d'enumeration des regles d'association frequentes.
 */
public class BruteForceAssociationRuleMiner extends AbstractAssociationRuleMiner {
	
	 private BooleanDatabase database;
    /**
     * Constructeur par defaut.
     * 
     * @param database base de donnees transactionnelles
     */
    public BruteForceAssociationRuleMiner(BooleanDatabase database) {
        super(database);
        this.database=database;
        //System.out.println(""+this.database.toString() + "=="+ database.toString());
    }

    @Override
   public Set<AssociationRule> extract(float minimalFrequency, float minimalConfidence) {
        
        ItemsetMiner setMiner = new Apriori(this.database);
        //System.out.println(""+this.database);
        Set<Itemset> frequentItemsets = setMiner.extract(minimalFrequency);
       //System.out.println("size of frequentItemsets :"+ setMiner.extract(minimalFrequency).size() );
        Set<AssociationRule> resultat = new HashSet<>();
        
        for(Itemset itemset : frequentItemsets) { 
            for(Set<BooleanVariable> premise : allCandidatePremises(itemset.getItems())) {
            	
                Set<BooleanVariable> conclusion = new HashSet<>(itemset.getItems());
                conclusion.removeAll(premise);    

                AssociationRule Rule = new AssociationRule(premise, conclusion, itemset.getFrequency(), AbstractAssociationRuleMiner.confidence(premise, conclusion, frequentItemsets));
                //System.out.println(""+Rule);
                if( Rule.getConfidence() >= minimalConfidence && Rule.getFrequency() >= minimalFrequency ) {                     
                    resultat.add(Rule);
                }

            }
            
        }
       

        return resultat;
    }

    /**
     * Retourne l'ensemble de tous ses sous-ensembles a l'exception de l'ensemble
     * vide et de l'ensemble lui-meme.
     * 
     * @param itemset motif
     * @return l'ensemble de ses sous-ensembles
     */
    public static Set<Set<BooleanVariable>> allCandidatePremises(Set<BooleanVariable> itemset){
        Set<Set<BooleanVariable>> candidate = new HashSet<Set<BooleanVariable>>();
        List<Set<BooleanVariable>> subset = new ArrayList<Set<BooleanVariable>>();

        for (BooleanVariable Variable :itemset)
        {
            Set<BooleanVariable> list=new HashSet<BooleanVariable>();
            list.add(Variable);
            subset.add(list);
            candidate.add(list);
        }
        while(!subset.isEmpty())
        {
            List<Set<BooleanVariable>> list=new ArrayList<Set<BooleanVariable>>();
            for(int i=0;i<subset.size();i++)
            {
                for(int j=i+1;j<subset.size();j++)
                {
                    Set<BooleanVariable> item = new HashSet<BooleanVariable>();
                    item.addAll(subset.get(i));
                    item.addAll(subset.get(j));
                    candidate.add(item);
                    if(!list.contains(item)){
                        list.add(item);
                    }
                }

            }
            subset=list;
        }
        candidate.remove(new HashSet<>());// retire l'ensemble vide
        candidate.remove(itemset);// retire l'ensemble lui-meme
        return candidate;
    }

	public BooleanDatabase getDatabase() {
		return database;
	}

	public void setDatabase(BooleanDatabase database) {
		this.database = database;
	}

	@Override
	public String toString() {
		return "BruteForceAssociationRuleMiner [database=" + this.getDatabase().toString() + "]";
	}
    
    
    
    
    
    
}
