package datamining;

import java.util.*;
import representation.*;

/**
 * Classe representant la base d'une implementation d'un mineur de donnees.
 */
public abstract class AbstractItemsetMiner implements ItemsetMiner {
    

   /**
     * Base de donnees transactionnelle booleenne.
     */
    
    protected BooleanDatabase database;
    
    /**
     * Permet de comparer deux variables booleennes entre elles.
     */
    
    public static final Comparator<BooleanVariable> COMPARATOR = 
    		(var1, var2) -> var1.getName().compareTo(var2.getName());
    		
   /**
     * Contructeur par defaut.
     * 
     * @param base base de donnees a utiliser
     */
    		
    public AbstractItemsetMiner(BooleanDatabase base) {
        this.database = base;
    }
    
    
    /**
     * Calcule la frequence d'un motif dans les transactions de la base de donnees.
     * 
     * @param itemset motif
     * @return frequence du motif dans la base de donnees
     */
    public float frequency(Set<BooleanVariable> itemset) {
        float compteur = 0;
        for (Set<BooleanVariable> Items : this.database.getTransactions()) {
            if (Items.containsAll(itemset)) {
                compteur++;
            }
        }
        return compteur / this.database.getTransactions().size();
    }

    public BooleanDatabase getDatabase() {
        return this.database;
    }


	@Override
	public String toString() {
		return "AbstractItemsetMiner [database=" + this.getDatabase().toString() + "]";
	}
    
    
}
