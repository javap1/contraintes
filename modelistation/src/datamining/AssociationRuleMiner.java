package datamining;

import java.util.*;

/**
 * Interface decrivant un extracteur de regles.
 */
public interface AssociationRuleMiner {
	 /**
     * Recupere la base de donnees transactionnelles.
     * 
     * @return base de donnees transactionnelles
     */
    public BooleanDatabase getDatabase();
    
    /**
     * Extrait des regles d'association frequemment retrouvees a partir des
     * transactions de la base de donnees transactionnelles.
     * 
     * @param minimalFrequency  frequence minimale des regles a prendre
     * @param minimalConfidence confiance minimale des regles a prendre
     * @return ensemble de regles d'association frequentes
     */
    public Set<AssociationRule> extract(float minimalFrequency, float minimalConfidence);
}
