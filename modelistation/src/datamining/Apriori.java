package datamining;


import java.util.*;

import representation.*;

/**
 * Classe representant un extracteur de donnees fonctionnant sur le principe de
 * l'algorithme 'apriori'.
 */
public class Apriori extends AbstractItemsetMiner {
	
	/**
     * Base de donnees transactionnelles.
     */
	private BooleanDatabase BooleanDatabase;

   /**
     * Contructeur par defaut.
     * 
     * @param database base de donnees a utiliser
     */
	
    public Apriori(BooleanDatabase database) {
        super(database);
    }
    
    /**
     * Recupere une base de donnees transactionnelle booleenne.
     * 
     * @return base de donnees
     */

    public BooleanDatabase getDatabase() {
        return this.BooleanDatabase;
    }

    /**
     * Recupere un ensemble de motifs frequemment trouves dans la base de donnees
     * transactionelle. L'algorithme ne renvoie que les motifs ayant une frequence
     * superieure ou egale a la minimale.
     * 
     * @param minimalFrequency frequence minimale pour la filtration
     * @return motifs frequents dans la base de donnees
     */
    public Set<Itemset> frequentSingletons(float minimalFrequency) {
    	
        Set<Itemset> itemsets = new HashSet<Itemset>();
        if(!(this.database==null))
        {
        for (BooleanVariable item : this.database.getItems()) {
           
            SortedSet<BooleanVariable> itemset = new TreeSet<>(AbstractItemsetMiner.COMPARATOR);
            itemset.add(item);

            float frequency = this.frequency(itemset);
            if (frequency >= minimalFrequency) { 
                itemsets.add(new Itemset(itemset, frequency));
            }
        }}
        return itemsets;
    }
    
    /**
     * Combine deux ensembles tries (par le comparateur
     * {@link AbstractItemsetMiner#COMPARATOR}) et un seul ensemble trie. Les deux
     * ensembles doivent avoir la meme taille k et les k-1 premiers elements doivent
     * etre les memes dans les deux ensembles.
     * 
     * @param set1 premier ensemble trie de variables
     * @param set2 second ensemble trie de variables
     * @return ensemble combinant les deux ensembles donnes en argument
     */
    
    public static SortedSet<BooleanVariable> combine(SortedSet<BooleanVariable> set1,SortedSet<BooleanVariable> set2) {
    	//Method1
    	if (set1.size() != set2.size()) {
            return null;
        }
        
    	if (set1.isEmpty() || set2.isEmpty()) {
            return null;
        }
        
        int k = set1.size();
        Iterator<BooleanVariable> set1Iterator = set1.iterator();
        Iterator<BooleanVariable> set2Iterator = set2.iterator();
        for (int i = 0; i < k - 1; i++) {
            if (!set1Iterator.next().equals(set2Iterator.next())) {
                return null;
            }
        }
        if (set1Iterator.next().equals(set2Iterator.next())) {
            return null;
        }
     
        
        SortedSet<BooleanVariable> toReturn = new TreeSet<>(AbstractItemsetMiner.COMPARATOR);
            toReturn.addAll(set1);
            toReturn.addAll(set2);
        
        return toReturn;
    
    	
    	
    	
    	
    	/*Method2
    	 * List<BooleanVariable> list1= new ArrayList<BooleanVariable>(set1);
       	 List<BooleanVariable> list2= new ArrayList<BooleanVariable>(set2);
         int k = set1.size();
         if(k==0)
        	return null;
    	if (set1.size() == set2.size() && !list1.get(k-1).equals(list2.get(k-1))) {
    		for (int i = 0; i < k - 1; i++)
   		      if (!list1.get(i).equals(list2.get(i))) 
                  return null;
    		SortedSet<BooleanVariable> toReturn = new TreeSet<>(AbstractItemsetMiner.COMPARATOR);
            toReturn.addAll(set1);
            toReturn.addAll(set2);
            return toReturn;
        }
    	return null;*/
        
    	
   }

    
    /**
     * Verifie que tous les sous-ensembles de variables d'un ensemble de variable
     * donne sont contenus dans une collection. Cette methode utilise la propriete
     * d'antimonotonie de la frequence assurant que les sous-ensembles plus petits
     * sont frequents.
     * 
     * @param items           ensemble de variables
     * @param itemsCollection collection d'ensemble de variables
     * @return booleen representant le fait que tous les sous-ensembles (de taille
     *         k-1) sont contenus dans la collection
     */
    
    public static boolean allSubsetsFrequent(Set<BooleanVariable> items,Collection<SortedSet<BooleanVariable>> itemsCollection) {
      
        for (BooleanVariable variable : items) {
            Set<BooleanVariable> items2 = new HashSet<>(items);
                                                                
            items2.remove(variable);
            
            if(!itemsCollection.contains(items2))
            	return false;
        }
        return true;
    }
    
    
    
    	  

		@Override
    public Set<Itemset> extract(float frequencyMin) {
        List<SortedSet<BooleanVariable>> pLevel = new ArrayList<>();
        Set<Itemset> singletons = this.frequentSingletons(frequencyMin);

        Set<Itemset> resultat = new HashSet<>(singletons);
        for (Itemset itemset : singletons)
        {
            SortedSet<BooleanVariable> ts = new TreeSet<>(AbstractItemsetMiner.COMPARATOR);
            ts.addAll(itemset.getItems());
            pLevel.add(ts);
        }
        if(!(this.database==null))
        {for (int i = 2 ; i <= this.database.getItems().size(); i++)
        {
            List<SortedSet<BooleanVariable>> nLevel = new ArrayList<>();
            for (int j = 0; j < pLevel.size(); j++)
            {
                for (int k = j+1; k < pLevel.size(); k++)
                {
                    SortedSet<BooleanVariable> booleanVariables = Apriori.combine(pLevel.get(j),pLevel.get(k));
                    if (booleanVariables != null && Apriori.allSubsetsFrequent(booleanVariables,pLevel)){

                        Itemset itemset = new Itemset(booleanVariables,this.frequency(booleanVariables));
                        if (itemset.getFrequency() >= frequencyMin)
                        {
                            nLevel.add(booleanVariables);
                            resultat.add(itemset);
                        }
                    }
                }
            }
            pLevel = nLevel;
        }
        }
        return resultat;
    }
    
		 @Override

 	    public String toString() {
 	        return "Apriori{" +
 	                "booleanDatabase=" + this.getDatabase().toString() +
 	                '}';
 	   }


}
