package datamining;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import representation.BooleanVariable;
import representation.Variable;


/**
 * Classe representant une base de donnees transactionnelles ou toutes les
 * variables sont booleennes.
 */
public class BooleanDatabase {
	
	/**
     * Ensemble de variables booleennes pouvant etre mis dans la base de donnees.
     */
    private Set<BooleanVariable> items;
    
    /**
     * Liste de transactions (instances).
     */
    private List<Set<BooleanVariable>> base;

    /**
     * Constructeur par defaut.
     * 
     * @param items ensemble de variables booleennes
     */
    public BooleanDatabase(Set<BooleanVariable> items) {
        this.items = items;
        this.base = new ArrayList<Set<BooleanVariable>>();
    }

    /**
     * Ajoute une transaction a la liste des transactions.
     * 
     * @param items transaction a ajouter
     * @see #base
     */
    public void add(Set<BooleanVariable> items) {
        this.base.add(items);
    }

    /**
     * Recupere l'ensemble des variables.
     * 
     * @return ensemble des variables
     * @see #items
     */
    public Set<BooleanVariable> getItems() {
        return this.items;
    }
    

    /**
     * Recupere la liste des transactions.
     * 
     * @return liste des transactions
     * @see #transactions
     */
    public List<Set<BooleanVariable>> getTransactions() {
        return this.base;
    }
    
    public String ItemtoString() {
		String s = new String();
		for (Variable v : this.getItems())
			s+=v.toString() + " ";
		return s;	
	
}
    
    public String basetoString() {
		String s = new String();
		for (Set<BooleanVariable> set : this.getTransactions())
			{for (Variable v : set)
				s+=v.toString() + " ";
			 s+="\n";
			}
	
		return s;	
	
}

	@Override
	public String toString() {
		return "BooleanDatabase [items=" + this.ItemtoString() + ", base=" + this.basetoString() + "]";
	}
    
    
    
    
}
